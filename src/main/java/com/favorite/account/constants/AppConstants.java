package com.favorite.account.constants;

/**
 * 
 * @author janbee
 *
 */
public class AppConstants {

private AppConstants() {
		super();
	}
	public static final String CUSTOMER_NOT_FOUND = "Customer not found";
	public static final int CUSTOMER_NOT_FOUND_STATUS_CODE = 1008;
	
	public static final String FAVORITE_ACCOUNT_EXISTS= "Favorite account exists already!";
	public static final int FAVORITE_ACCOUNT_EXISTS_STATUS_CODE = 1009;
	
	public static final String FAVORITE_ACCOUNT_ADDED = "Favorite account added successfully";
	public static final String FAVORITE_ACCOUNT_ADDED_STATUS_CODE ="1004";
	
	public static final String MAX_FAVORITE_ACCOUNT_EXCEEDED= "Max Favorite accounts exceeded.";
	public static final int MAX_FAVORITE_ACCOUNT_EXCEEDED_STATUS_CODE = 1010;
	
	public static final String BANK_NAME ="bankName";
	public static final String ACCOUNT_INFO_URL="bankAccountServiceURL";
	public static final String OWNER_NAME = "ownerName";
	public static final String LOCAL_HOST = "localhost";
	public static final String DNS_NAME = "pip-cnc-java-ui1221015.eastus.cloudapp.azure.com";
	
	public static final String BANK_INFO_URL = "http://pip-cnc-java-ui1221015.eastus.cloudapp.azure.com:9080/api/bankinfo/";
	
	public static final String LOGIN_SUCCESS = "Login successful";
	public static final String LOGIN_SUCCESS_STATUS_CODE = "600";
	public static final String LOGIN_FAILURE = "login failed, please enter valid credentials";
	public static final String LOGIN_FAILURE_STATUS_CODE = "601";

	public static final String FAVOURITE_ACCOUNT_STATUS_CODE="1001";
	public static final String FAVOURITE_ACCOUNT_STATUS_MSG="NO FAVOURITE ACCOUNT ADDED";
	
	public static final String FAVOURITE_ACCOUNT_DELETE_STATUS_CODE="1006";
	public static final String FAVOURITE_ACCOUNT_DELETE_STATUS_MSG="CUSTOMER FAVOURITE ACCOUNt DELETED SUCCESSFULLY";
	
	public static final String FAVOURITE_ACCOUNT_NOT_FOUND_CODE="1007";
	public static final String FAVOURITE_ACCOUNT_NOT_FOUND_MSG="NO FAVORITE ACCOUNTS FOUND";

}
