package com.favorite.account.exception;

public class CustomerAccountNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerAccountNotFoundException() {
		super();
	}

	public CustomerAccountNotFoundException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public CustomerAccountNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CustomerAccountNotFoundException(String arg0) {
		super(arg0);
	}

	public CustomerAccountNotFoundException(Throwable arg0) {
		super(arg0);
	}
	
	
}
