package com.favorite.account.exception;

/**
 * 
 * @author janbee
 *
 */
public class GeneralException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * GeneralException constructor
	 */
	public GeneralException() {
		super();
	}

	/**
	 * 
	 * @param message
	 */
	public GeneralException(String message) {
		super(message);
	}
	
}
