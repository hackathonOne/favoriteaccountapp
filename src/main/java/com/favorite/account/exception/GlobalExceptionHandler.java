package com.favorite.account.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.favorite.account.constants.AppConstants;

/**
 * 
 * @author janbee
 *
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 
	 * @return ErrorResponse
	 */
	@ExceptionHandler(value = CustomerNotFoundException.class)
	public ResponseEntity<ExceptionResponse> handleCustomerNotFoundException() {
		ExceptionResponse error = new ExceptionResponse();
		error.setStatus(AppConstants.CUSTOMER_NOT_FOUND_STATUS_CODE);
		error.setMessage(AppConstants.CUSTOMER_NOT_FOUND);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
	
	/**
	 * 
	 * @return ErrorResponse
	 */
	@ExceptionHandler(value = FavoriteAccountExistsException.class)
	public ResponseEntity<ExceptionResponse> handleFavoriteAccountExistsException() {
		ExceptionResponse error = new ExceptionResponse();
		error.setStatus(AppConstants.FAVORITE_ACCOUNT_EXISTS_STATUS_CODE);
		error.setMessage(AppConstants.FAVORITE_ACCOUNT_EXISTS);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
	
	/**
	 * 
	 * @return ErrorResponse
	 */
	@ExceptionHandler(value = MaxFavoriteAccountException.class)
	public ResponseEntity<ExceptionResponse> handleMaxFavoriteAccountException() {
		ExceptionResponse error = new ExceptionResponse();
		error.setStatus(AppConstants.MAX_FAVORITE_ACCOUNT_EXCEEDED_STATUS_CODE);
		error.setMessage(AppConstants.MAX_FAVORITE_ACCOUNT_EXCEEDED);
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
}
