package com.favorite.account.exception;

/**
 * 
 * @author janbee
 *
 */
public class MaxFavoriteAccountException extends GeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * MaxFavoriteAccountException constructor
	 */
	public MaxFavoriteAccountException() {
		super();
	}

	/**
	 * 
	 * @param message
	 */
	public MaxFavoriteAccountException(String message) {
		super(message);
	}

	
}
