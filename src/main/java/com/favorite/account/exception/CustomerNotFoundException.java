package com.favorite.account.exception;

/**
 * 
 * @author janbee
 *
 */
public class CustomerNotFoundException extends GeneralException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * CustomerNotFoundException constructor
	 */
	public CustomerNotFoundException() {
		super();
	}

	/**
	 * 
	 * @param message
	 */
	public CustomerNotFoundException(String message) {
		super(message);
	}

}
