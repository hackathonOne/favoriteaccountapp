package com.favorite.account.exception;

/**
 * 
 * @author janbee
 *
 */
public class FavoriteAccountExistsException extends GeneralException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * FavoriteAccountExistsException constructor
	 */
	public FavoriteAccountExistsException() {
		super();
	}

	/**
	 * 
	 * @param message
	 */
	public FavoriteAccountExistsException(String message) {
		super(message);
	}

}
