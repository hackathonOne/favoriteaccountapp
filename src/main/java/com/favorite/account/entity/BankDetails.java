package com.favorite.account.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Entity
@Table(name = "BANK_DETAILS")
@Setter
@Getter
public class BankDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer bankId;
	private String bankName;
	/**
	 * BankDetails constructor
	 */
	public BankDetails() {
		super();
	}
	/**
	 * 
	 * @param bankId
	 * @param bankName
	 */
	public BankDetails(Integer bankId, String bankName) {
		super();
		this.bankId = bankId;
		this.bankName = bankName;
	}
	@Override
	public String toString() {
		return "BankDetails [bankId=" + bankId + ", bankName=" + bankName + "]";
	}
	
	
}
