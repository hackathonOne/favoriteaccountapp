package com.favorite.account.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Entity
@Table(name = "CUSTOMER_FAVORITE_ACCOUNT")
@Setter
@Getter
public class CustomerFavoriteAccount implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer favoriteAccountId;
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
	@ManyToOne
	@JoinColumn(name = "accountId")
	private Account account;
	/**
	 * CustomerFavoriteAccount constructor
	 */
	public CustomerFavoriteAccount() {
		super();
	}
	/**
	 * 
	 * @param favoriteAccountId
	 * @param customer
	 * @param account
	 */
	public CustomerFavoriteAccount(Integer favoriteAccountId, Customer customer, Account account) {
		super();
		this.favoriteAccountId = favoriteAccountId;
		this.customer = customer;
		this.account = account;
	}
	@Override
	public String toString() {
		return "CustomerFavoriteAccount [favoriteAccountId=" + favoriteAccountId + ", customer=" + customer
				+ ", account=" + account + "]";
	}
	
	
}
