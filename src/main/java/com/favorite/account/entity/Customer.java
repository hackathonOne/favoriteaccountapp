package com.favorite.account.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Entity
@Table(name = "CUSTOMER")
@Setter
@Getter
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CUSTOMER_ID")
	@NotNull(message = "customerId is mandatory")
	private Integer customerId;
	@NotEmpty(message = "customer name is mandatory")
	@Column(name = "CUSTOMER_NAME")
	private String customerName;
	/**
	 * Customer constructor
	 */
	public Customer() {
		super();
	}
	/**
	 * 
	 * @param customerId
	 * @param customerName
	 */
	public Customer(Integer customerId, @NotEmpty(message = "customer name is mandatory") String customerName) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + "]";
	}

}
