package com.favorite.account.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Entity
@Table(name = "ACCOUNT")
@Setter
@Getter
public class Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer accountId;
	private String accountOwner;
	private String iban;
	@ManyToOne
	@JoinColumn(name = "bankId")
	private BankDetails bankDetails;
	/**
	 * Account constructor
	 */
	public Account() {
		super();
	}
	/**
	 * 
	 * @param accountId
	 * @param accountOwner
	 * @param iBan
	 * @param bankDetails
	 */
	public Account(Integer accountId, String accountOwner, String iBan, BankDetails bankDetails) {
		super();
		this.accountId = accountId;
		this.accountOwner = accountOwner;
		this.iban = iBan;
		this.bankDetails = bankDetails;
	}
	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", accountOwner=" + accountOwner + ", iBan=" + iban
				+ ", bankDetails=" + bankDetails + "]";
	}
	
}
