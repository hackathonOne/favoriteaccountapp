package com.favorite.account.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Setter
@Getter
public class AddRequestDto {

	private String iban;

	/**
	 *  AddRequestDto constructor
	 */
	public AddRequestDto() {
		super();
	}

	/**
	 * 
	 * @param iban
	 */
	public AddRequestDto(String iban) {
		super();
		this.iban = iban;
	}
	
}
