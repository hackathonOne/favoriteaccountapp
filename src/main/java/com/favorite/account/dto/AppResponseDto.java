package com.favorite.account.dto;

public class AppResponseDto {

	private String statusCode;
	private String statusMsg;
	public String getStatusCode() {
		return statusCode;
	}
	
	public AppResponseDto() {
		super();
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	
}
