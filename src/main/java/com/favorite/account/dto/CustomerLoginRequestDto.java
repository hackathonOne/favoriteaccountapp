package com.favorite.account.dto;

import javax.validation.constraints.NotNull;

/**
 * @author prabirkumar.jena
 *
 */
public class CustomerLoginRequestDto {
	@NotNull(message = "customerId is mandatory")
	private Integer customerId;

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

}
