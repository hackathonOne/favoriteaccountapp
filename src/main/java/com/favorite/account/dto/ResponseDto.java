package com.favorite.account.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author janbee
 *
 */
@Setter
@Getter
public class ResponseDto {

	private String statusMsg;
	private String statusCode;
	/**
	 * ResponseDto constructor
	 */
	public ResponseDto() {
		super();
	}
	/**
	 * 
	 * @param statusMsg
	 * @param statusCode
	 */
	public ResponseDto(String statusMsg, String statusCode) {
		super();
		this.statusMsg = statusMsg;
		this.statusCode = statusCode;
	}
	
}
