package com.favorite.account.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.favorite.account.constants.AppConstants;
import com.favorite.account.dto.AddRequestDto;
import com.favorite.account.dto.AppResponseDto;
import com.favorite.account.dto.CustomerFavAccountDetailDto;
import com.favorite.account.dto.CustomerLoginRequestDto;
import com.favorite.account.dto.CustomerLoginResponseDto;
import com.favorite.account.dto.ResponseDto;
import com.favorite.account.entity.Account;
import com.favorite.account.entity.BankDetails;
import com.favorite.account.entity.Customer;
import com.favorite.account.entity.CustomerFavoriteAccount;
import com.favorite.account.exception.CustomerAccountNotFoundException;
import com.favorite.account.exception.CustomerNotFoundException;
import com.favorite.account.exception.FavoriteAccountExistsException;
import com.favorite.account.exception.MaxFavoriteAccountException;
import com.favorite.account.repository.AccountRepository;
import com.favorite.account.repository.BankDetailsRepository;
import com.favorite.account.repository.CustomerFavoriteAaccRepository;
import com.favorite.account.repository.CustomerFavoriteAccountRepository;
import com.favorite.account.repository.CustomerRepository;


@Service
public class CustomerServiceImpl implements CustomerService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	private CustomerFavoriteAaccRepository customerFavRepo;
	
	@Autowired
	BankDetailsRepository bankDetailsRepository;
	@Autowired
	CustomerFavoriteAccountRepository favoriteAccountRepository;

	@Override
	public Object getCustomerFavoriteAccountDetails(Integer customerId, Integer pageNo, Integer pageSize) throws CustomerAccountNotFoundException {
		LOGGER.info("*************Inside getCustomerFavoriteAccountDetails ************* START ");
		AppResponseDto respnse = new AppResponseDto();
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		List<CustomerFavAccountDetailDto> rresponseList = new ArrayList<>();
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		List<CustomerFavoriteAccount> customerFavAccout = customerFavRepo.findByCustomer(customer, pageable);
		if(!customerFavAccout.isEmpty()) {
			LOGGER.info("!!!! User Have Favorite accounts!!!!!!!!!!!!!");
			for (CustomerFavoriteAccount customerFavoriteAccount : customerFavAccout) {
				CustomerFavAccountDetailDto response = new CustomerFavAccountDetailDto();
				response.setOwnerName(customerFavoriteAccount.getAccount().getAccountOwner());
				response.setIban(customerFavoriteAccount.getAccount().getIban());
				response.setBankName(customerFavoriteAccount.getAccount().getBankDetails().getBankName());
				rresponseList.add(response);
			}
		}else {
			respnse.setStatusCode(AppConstants.FAVOURITE_ACCOUNT_STATUS_CODE);
			respnse.setStatusMsg(AppConstants.FAVOURITE_ACCOUNT_STATUS_MSG);
			return respnse;

		}
		return rresponseList;

	}

	public Object deleteFavAccountDetails(Integer customerId, Integer favAccId) {
		LOGGER.info("Inside deleteFavAccountDetails ");
		AppResponseDto respnse = new AppResponseDto();
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		List<CustomerFavoriteAccount> customerFavAccout = customerFavRepo.findByCustomer(customer);
		List<Integer> favAccIds = customerFavAccout.stream().map(favAcc-> favAcc.getFavoriteAccountId()).collect(Collectors.toList());
		if(!favAccIds.isEmpty() && favAccIds.contains(favAccId)) {
			customerFavRepo.deleteById(favAccId);
			respnse.setStatusCode(AppConstants.FAVOURITE_ACCOUNT_DELETE_STATUS_CODE);
			respnse.setStatusMsg(AppConstants.FAVOURITE_ACCOUNT_DELETE_STATUS_MSG);
			return respnse;
		}else {
			LOGGER.debug("NO FAVORITE ACCOUNT FOUND TO DELETE");
			respnse.setStatusCode(AppConstants.FAVOURITE_ACCOUNT_NOT_FOUND_CODE);
			respnse.setStatusMsg(AppConstants.FAVOURITE_ACCOUNT_NOT_FOUND_MSG);
			return respnse;
		}
	}

	@Override
	public ResponseDto addFavoriteAccount(AddRequestDto requestDto,Integer customerId) throws FavoriteAccountExistsException, CustomerNotFoundException, MaxFavoriteAccountException {
		LOGGER.info("CustomerServiceImpl :: addFavoriteAccount :: start");
		String iban = requestDto.getIban();
		Optional<Account> account =null;
		String accountServiceUrl = "";
		Optional<Customer> customer = customerRepository.findById(customerId);
		if(!customer.isPresent()) {
			throw new CustomerNotFoundException();
		}
		Customer custObj = customer.get();
		List<CustomerFavoriteAccount> favoriteAccList = favoriteAccountRepository.findByCustomer(custObj);
		if(favoriteAccList.size()==9) {
			throw new MaxFavoriteAccountException();
		}

		account = accountRepository.findByIban(iban);

		BankDetails bankDetails = new BankDetails();
		//calling external service to get bank info
		JSONObject bankInfo  = getBankName(iban);
		if(bankInfo.has(AppConstants.BANK_NAME)) {
			bankDetails.setBankName(bankInfo.getString(AppConstants.BANK_NAME));
		}
		if(bankInfo.has(AppConstants.ACCOUNT_INFO_URL)) {
			accountServiceUrl = bankInfo.getString(AppConstants.ACCOUNT_INFO_URL);
		}
		BankDetails bank = bankDetailsRepository.save(bankDetails);


		//calling external service to get account info
		JSONObject accountInfo = getAccountOwner(accountServiceUrl,iban);
		Account newAccount = null;
		if(!account.isPresent()) {
			Account accountObj = new Account();
			if(accountInfo.has(AppConstants.OWNER_NAME)) {
				accountObj.setAccountOwner(accountInfo.getString(AppConstants.OWNER_NAME));
			}
			accountObj.setIban(iban);
			accountObj.setBankDetails(bank);
			newAccount = accountRepository.save(accountObj);
		}
		Account acc;
		if(!account.isPresent()) {
			acc = newAccount;
		}  else {
			acc = account.get();
		}

		CustomerFavoriteAccount favoriteAcc = favoriteAccountRepository.findByCustomerAndAccount(custObj, acc);
		if(favoriteAcc!=null) {
			throw new FavoriteAccountExistsException();
		}

		CustomerFavoriteAccount favoriteAccObj = new CustomerFavoriteAccount();
		favoriteAccObj.setAccount(acc);
		favoriteAccObj.setCustomer(custObj);
		favoriteAccountRepository.save(favoriteAccObj);

		ResponseDto response = new ResponseDto();
		response.setStatusCode(AppConstants.FAVORITE_ACCOUNT_ADDED_STATUS_CODE);
		response.setStatusMsg(AppConstants.FAVORITE_ACCOUNT_ADDED);
		LOGGER.info("CustomerServiceImpl :: addFavoriteAccount :: end");
		return response;
	}

	private JSONObject getAccountOwner(String url, String iban) {
		RestTemplate restTemplate = new RestTemplate();
		String url1 = url.replace(AppConstants.LOCAL_HOST, AppConstants.DNS_NAME);
		url1 = url1 +"/"+iban;
		String str = restTemplate.getForObject(url1,String.class);
		return new JSONObject(str);
	}

	private JSONObject getBankName(String iban) {
		RestTemplate restTemplate = new RestTemplate();
		String url = AppConstants.BANK_INFO_URL+iban;
		String str = restTemplate.getForObject(url, String.class);
		return new JSONObject(str);
	}

	/**
	 * @return CustomerLoginResponseDto
	 * 
	 * @param CustomerLoginRequestDto
	 */
	@Override
	public CustomerLoginResponseDto customerLogin(CustomerLoginRequestDto reqDto) {
		LOGGER.info("Inside CustomerServiceImpl customerLogin() method");
		CustomerLoginResponseDto response = new CustomerLoginResponseDto();
		Customer customer = customerRepository.findByCustomerId(reqDto.getCustomerId());
		if (customer != null) {
			response.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
			response.setStatusMsg(AppConstants.LOGIN_SUCCESS);
		} else {
			response.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
			response.setStatusMsg(AppConstants.CUSTOMER_NOT_FOUND);
		}
		LOGGER.info("End of CustomerServiceImpl customerLogin() method");
		return response;
	}
}



