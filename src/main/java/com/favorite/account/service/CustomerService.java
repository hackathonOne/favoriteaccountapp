package com.favorite.account.service;

import com.favorite.account.dto.AddRequestDto;
import com.favorite.account.dto.CustomerLoginRequestDto;
import com.favorite.account.dto.CustomerLoginResponseDto;
import com.favorite.account.dto.ResponseDto;
import com.favorite.account.exception.CustomerAccountNotFoundException;
import com.favorite.account.exception.CustomerNotFoundException;
import com.favorite.account.exception.FavoriteAccountExistsException;
import com.favorite.account.exception.GeneralException;

/**
 * 
 * @author janbee
 *
 */
public interface CustomerService {

	/**
	 * 
	 * @param requestDto
	 * @param customerId
	 * @return ResponseDto
	 * @throws FavoriteAccountExistsException
	 * @throws CustomerNotFoundException
	 */
	public ResponseDto addFavoriteAccount(AddRequestDto requestDto,Integer customerId) throws GeneralException;

	/**
	 * 
	 * @param reqDto
	 * @return CustomerLoginResponseDto
	 */
	public CustomerLoginResponseDto customerLogin(CustomerLoginRequestDto reqDto);
	
	/**
	 * 
	 * @param customerId
	 * @param pageNo
	 * @param pageSize
	 * @return Object
	 * @throws CustomerAccountNotFoundException
	 */
	public Object getCustomerFavoriteAccountDetails(Integer customerId, Integer pageNo, Integer pageSize) throws CustomerAccountNotFoundException;
}
