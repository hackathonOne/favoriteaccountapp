package com.favorite.account.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.favorite.account.dto.AddRequestDto;
import com.favorite.account.dto.CustomerLoginRequestDto;
import com.favorite.account.dto.CustomerLoginResponseDto;
import com.favorite.account.dto.ResponseDto;
import com.favorite.account.exception.CustomerAccountNotFoundException;
import com.favorite.account.exception.GeneralException;
import com.favorite.account.service.CustomerService;
import com.favorite.account.service.CustomerServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/customers")
@Api("Operations pertaining to customer service")
public class CustomerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	private CustomerServiceImpl customerServiceImpl;

	@Autowired
	CustomerService customerService;
	
	/**
	 * 
	 * @param requestDto
	 * @param customerId
	 * @return ResponseDto
	 * @throws GeneralException 
	 */
	@PostMapping("/{customerId}/customerFavoriteAccounts")
	@ApiOperation("Adds favorite account")
	@ApiResponses({@ApiResponse(code = 1008,message = "Customer not found")
	,@ApiResponse(code = 1009,message = "Favorite account exists already!"),
	@ApiResponse(code = 1010,message = "Max Favorite accounts exceeded.")})
	public ResponseEntity<ResponseDto> addFavoriteAccount(@RequestBody AddRequestDto requestDto, @PathVariable("customerId") Integer customerId) throws GeneralException {
		LOGGER.debug("CustomerController :: addFavoriteAccount:: start ");
		ResponseDto response = customerService.addFavoriteAccount(requestDto, customerId);
		return new ResponseEntity<>(response,HttpStatus.CREATED);
	}
	/**
	 * 
	 * @param CustomerLoginRequestDto
	 * @return customerLogin
	 */
	@PostMapping("/login")
	public ResponseEntity<CustomerLoginResponseDto> customerLogin(@Valid @RequestBody CustomerLoginRequestDto reqDto) {
		LOGGER.info("CustomerController customerLogin() called for login ");
		return new ResponseEntity<>(customerServiceImpl.customerLogin(reqDto), HttpStatus.OK);
	}
	

	@GetMapping("{customerId}/customerFavoriteAccounts")
	@ApiOperation("Fetching customer favourite account details")
	@ApiResponses(value = { @ApiResponse(code = 1003, message = "NO FAVOURITE ACCOUNT ADDED") })
	public ResponseEntity<Object> getCustomerAccountDetails(@PathVariable("customerId") Integer customerId,
			@RequestParam("pageNo") Integer pageNo,@RequestParam("pageSize") Integer pageSize) throws CustomerAccountNotFoundException {
		Object favoriteAccoutsResponse =  customerServiceImpl.getCustomerFavoriteAccountDetails(customerId, pageNo, pageSize);
		return new ResponseEntity<>(favoriteAccoutsResponse, HttpStatus.OK);
	}

	@DeleteMapping("/{customerId}/customerFavoriteAccounts/{favAccId}")
	@ApiOperation("Deleting the customer favourite account by favourite account Id")
	@ApiResponses(value = { @ApiResponse(code = 1006, message = "CUSTOMER FAVOURITE ACCOUNt DELETED SUCCESSFULLY"),
			@ApiResponse(code = 1007, message = "NO FAVOURITE CUSTOMER FOUND TO DELETE")})
	public ResponseEntity<Object> deleteFavAccountDetails(@PathVariable("customerId") Integer customerId,
			@PathVariable("favAccId") Integer favAccId) {
		Object response = customerServiceImpl.deleteFavAccountDetails(customerId, favAccId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
