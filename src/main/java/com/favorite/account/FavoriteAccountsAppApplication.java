package com.favorite.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author janbee
 *
 */
@SpringBootApplication
public class FavoriteAccountsAppApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(FavoriteAccountsAppApplication.class, args);
	}

}
