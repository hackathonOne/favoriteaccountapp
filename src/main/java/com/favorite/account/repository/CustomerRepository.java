package com.favorite.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.favorite.account.entity.Customer;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	public Customer findByCustomerId(Integer customerId);

}
