package com.favorite.account.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.favorite.account.entity.Customer;
import com.favorite.account.entity.CustomerFavoriteAccount;

@Repository
public interface CustomerFavoriteAaccRepository extends JpaRepository<CustomerFavoriteAccount, Integer>{

	List<CustomerFavoriteAccount> findByCustomer(Customer customer, Pageable pageable);

	List<CustomerFavoriteAccount> findByCustomer(Customer customer);
}
