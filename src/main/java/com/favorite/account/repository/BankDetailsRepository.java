package com.favorite.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.favorite.account.entity.BankDetails;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface BankDetailsRepository extends JpaRepository<BankDetails, Integer>{

}
