package com.favorite.account.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.favorite.account.entity.Account;
import com.favorite.account.entity.Customer;
import com.favorite.account.entity.CustomerFavoriteAccount;
/**
 * 
 * @author janbee
 *
 */
@Repository
public interface CustomerFavoriteAccountRepository extends JpaRepository<CustomerFavoriteAccount, Integer>{

	/**
	 * 
	 * @param customer
	 * @param account
	 * @return CustomerFavoriteAccount
	 */
	public CustomerFavoriteAccount findByCustomerAndAccount(Customer customer, Account account);
	
	/**
	 * 
	 * @param customer
	 * @return list of CustomerFavoriteAccounts
	 */
	public List<CustomerFavoriteAccount> findByCustomer(Customer customer);
	
	List<CustomerFavoriteAccount> findByCustomer(Customer customer, Pageable pageable);

}
