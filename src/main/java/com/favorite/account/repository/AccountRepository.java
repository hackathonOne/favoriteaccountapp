package com.favorite.account.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.favorite.account.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

	/**
	 * 
	 * @param iban
	 * @return Account
	 */
	Optional<Account> findByIban(String iban);

	/**
	 * 
	 * @param account
	 * @return Account
	 */
	Account findByBankDetails(Account account);

	
}
