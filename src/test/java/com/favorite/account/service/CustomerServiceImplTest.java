package com.favorite.account.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.favorite.account.constants.AppConstants;
import com.favorite.account.dto.AddRequestDto;
import com.favorite.account.dto.CustomerLoginRequestDto;
import com.favorite.account.dto.CustomerLoginResponseDto;
import com.favorite.account.entity.Account;
import com.favorite.account.entity.Customer;
import com.favorite.account.entity.CustomerFavoriteAccount;
import com.favorite.account.exception.CustomerAccountNotFoundException;
import com.favorite.account.exception.CustomerNotFoundException;
import com.favorite.account.exception.FavoriteAccountExistsException;
import com.favorite.account.exception.MaxFavoriteAccountException;
import com.favorite.account.repository.AccountRepository;
import com.favorite.account.repository.BankDetailsRepository;
import com.favorite.account.repository.CustomerFavoriteAccountRepository;
import com.favorite.account.repository.CustomerRepository;

class CustomerServiceImplTest {

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;
	
	@Mock
	CustomerRepository customerRepository;
	
	@Mock
	AccountRepository accountRepository;
	
	@Mock
	BankDetailsRepository bankDetailsRepository;
	
	@Mock
	CustomerFavoriteAccountRepository favoriteAccountRepository;
	
	@Mock
	private Pageable pageable;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	/*@Test
	public void testAddFavoriteAccount()  {
		Integer customerId = 1;
		Customer cust = new Customer();
		Optional<Customer> customer = Optional.of(cust);
		AddRequestDto request = new AddRequestDto();
		request.setIban("FR33123600033300002");
		try {
		doReturn(customer).when(customerRepository).findById(customerId);
		customerServiceImpl.addFavoriteAccount(request, customerId);
		}catch(CustomerNotFoundException | FavoriteAccountExistsException e) {
			e.printStackTrace();
		}
	}*/
	
	
	@Test
	public void testAddFavoriteAccount1()  {
		Integer customerId = 1;
		Customer cust = new Customer();
		cust.setCustomerId(1);
		AddRequestDto request = new AddRequestDto();
		request.setIban("FR33123600033300002");
		Account account = new Account();
		account.setAccountId(1);
		account.setIban("FR33123600033300002");
		
		try {
		doReturn(Optional.of(cust)).when(customerRepository).findById(customerId);
		assertEquals(1, cust.getCustomerId());
		doReturn(Optional.of(account)).when(accountRepository).findByIban(request.getIban());
		customerServiceImpl.addFavoriteAccount(request, customerId);
		}catch(CustomerNotFoundException | FavoriteAccountExistsException | MaxFavoriteAccountException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddFavoriteAccount2() throws FavoriteAccountExistsException, CustomerNotFoundException, MaxFavoriteAccountException  {
		Integer customerId = 1;
		Customer cust = new Customer();
		cust.setCustomerId(1);
		AddRequestDto request = new AddRequestDto();
		request.setIban("FR33123600033300002");
		Account account =new Account();
		doReturn(Optional.of(cust)).when(customerRepository).findById(customerId);
		assertEquals(1, cust.getCustomerId());
		when(accountRepository.findByIban(request.getIban())).thenReturn(Optional.of(account));	
		customerServiceImpl.addFavoriteAccount(request, customerId);
	}

	@Test
	public void testLoginValidationSuccess() {
		CustomerLoginRequestDto customerLoginReqDto = new CustomerLoginRequestDto();
		customerLoginReqDto.setCustomerId(1);

		Customer customer = new Customer();
		customer.setCustomerId(1);
		when(customerRepository.findByCustomerId(customer.getCustomerId())).thenReturn(customer);

		CustomerLoginResponseDto customerLoginResponseDto = customerServiceImpl.customerLogin(customerLoginReqDto);
		assertEquals(AppConstants.LOGIN_SUCCESS, customerLoginResponseDto.getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, customerLoginResponseDto.getStatusCode());
	}

	@Test
	public void testLoginValidationFailure() {
		CustomerLoginRequestDto customerLoginReqDto = new CustomerLoginRequestDto();
		customerLoginReqDto.setCustomerId(1);

		Customer customer = new Customer();
		when(customerRepository.findByCustomerId(customer.getCustomerId())).thenReturn(null);

		CustomerLoginResponseDto customerLoginResponseDto = customerServiceImpl.customerLogin(customerLoginReqDto);
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, customerLoginResponseDto.getStatusCode());
		assertEquals(AppConstants.CUSTOMER_NOT_FOUND, customerLoginResponseDto.getStatusMsg());
	}
	@Test
	public void testgetCustomerFavoriteAccountDetails() throws CustomerAccountNotFoundException {
		List<CustomerFavoriteAccount> customerFavAccout = new ArrayList<CustomerFavoriteAccount>();
		Pageable pageable =  PageRequest.of(0, 2);
		CustomerFavoriteAccount fav = new CustomerFavoriteAccount();
		Account account = new Account();
		account.setAccountId(1);
		Customer customer = new Customer();
		customer.setCustomerId(1);
		fav.setAccount(account);
		fav.setFavoriteAccountId(1);
		fav.setCustomer(customer);
		customerFavAccout.add(fav);
		doReturn(customerFavAccout).when(favoriteAccountRepository).findByCustomer(customer, pageable);
		customerServiceImpl.getCustomerFavoriteAccountDetails(1, 0, 2);
		assertEquals("1", customerFavAccout.get(0).getFavoriteAccountId().toString());
	}
	
	@Test
	public void testgetCustomerFavoriteAccountDetailsElse() throws CustomerAccountNotFoundException {
		List<CustomerFavoriteAccount> customerFavAccout = new ArrayList<CustomerFavoriteAccount>();
		CustomerFavoriteAccount favorite = new CustomerFavoriteAccount();
		favorite.setFavoriteAccountId(1);
		Account account = new Account();
		account.setAccountId(1);
		Customer customer = new Customer();
		customer.setCustomerId(1);
		customerFavAccout.add(favorite);
		doReturn(customerFavAccout).when(favoriteAccountRepository).findByCustomer(customer, pageable);
		customerServiceImpl.getCustomerFavoriteAccountDetails(1, 0, 2);
		assertEquals("1", customerFavAccout.get(0).getFavoriteAccountId().toString());
	}
	
	@Test
	public void testdeleteFavAccountDetails() {
		Customer customer = new Customer();
		customer.setCustomerId(1);
		List<CustomerFavoriteAccount> customerFavAccout = new ArrayList<>();
		CustomerFavoriteAccount fav = new CustomerFavoriteAccount();
		fav.setFavoriteAccountId(1);
		customerFavAccout.add(fav);
		doReturn(customerFavAccout).when(favoriteAccountRepository).findByCustomer(customer);
		customerServiceImpl.deleteFavAccountDetails(1, 1);
		assertEquals("1", customerFavAccout.get(0).getFavoriteAccountId().toString());
	}
	
}


