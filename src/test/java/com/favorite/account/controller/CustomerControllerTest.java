package com.favorite.account.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import com.favorite.account.constants.AppConstants;
import com.favorite.account.dto.AddRequestDto;
import com.favorite.account.dto.AppResponseDto;
import com.favorite.account.dto.CustomerLoginRequestDto;
import com.favorite.account.dto.CustomerLoginResponseDto;
import com.favorite.account.dto.ResponseDto;
import com.favorite.account.entity.Account;
import com.favorite.account.entity.Customer;
import com.favorite.account.entity.CustomerFavoriteAccount;
import com.favorite.account.exception.CustomerAccountNotFoundException;
import com.favorite.account.exception.GeneralException;
import com.favorite.account.service.CustomerServiceImpl;

class CustomerControllerTest {

	@InjectMocks
	CustomerController customerController;
	@Mock
	CustomerServiceImpl customerServiceImpl;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testaddFavoriteAccount() throws GeneralException {
		Integer customerId = 1;
		AddRequestDto request = new AddRequestDto();
		request.setIban("FR33123600033300002");

		ResponseDto response = new ResponseDto();
		response.setStatusCode(AppConstants.FAVORITE_ACCOUNT_ADDED_STATUS_CODE);
		response.setStatusMsg(AppConstants.FAVORITE_ACCOUNT_ADDED);

		doReturn(response).when(customerServiceImpl).addFavoriteAccount(request, customerId);
		customerController.addFavoriteAccount(request, customerId);
		assertEquals("1004", response.getStatusCode());
	}

	@Test
	public void testUserLoginSuccess() {
		CustomerLoginRequestDto customerLoginReqDto = new CustomerLoginRequestDto();
		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginReqDto.setCustomerId(1);
		customerLoginResponseDto.setStatusCode(AppConstants.LOGIN_SUCCESS_STATUS_CODE);
		customerLoginResponseDto.setStatusMsg(AppConstants.LOGIN_SUCCESS);
		doReturn(customerLoginResponseDto).when(customerServiceImpl).customerLogin(customerLoginReqDto);
		ResponseEntity<CustomerLoginResponseDto> customerLoginResponseDto1 = customerController
				.customerLogin(customerLoginReqDto);

		assertEquals(AppConstants.LOGIN_SUCCESS, customerLoginResponseDto1.getBody().getStatusMsg());
		assertEquals(AppConstants.LOGIN_SUCCESS_STATUS_CODE, customerLoginResponseDto1.getBody().getStatusCode());
	}

	@Test
	public void testUserLoginFailure() {
		CustomerLoginRequestDto customerLoginReqDto = new CustomerLoginRequestDto();
		CustomerLoginResponseDto customerLoginResponseDto = new CustomerLoginResponseDto();
		customerLoginResponseDto.setStatusCode(AppConstants.LOGIN_FAILURE_STATUS_CODE);
		customerLoginResponseDto.setStatusMsg(AppConstants.LOGIN_FAILURE);
		doReturn(customerLoginResponseDto).when(customerServiceImpl).customerLogin(customerLoginReqDto);
		ResponseEntity<CustomerLoginResponseDto> customerLoginResponseDto1 = customerController
				.customerLogin(customerLoginReqDto);

		assertEquals(AppConstants.LOGIN_FAILURE, customerLoginResponseDto1.getBody().getStatusMsg());
		assertEquals(AppConstants.LOGIN_FAILURE_STATUS_CODE, customerLoginResponseDto1.getBody().getStatusCode());
	}
	@Test
	public void testgetCustomerFavoriteAccountDetails() {
		List<CustomerFavoriteAccount> customerFavAccout = new ArrayList<CustomerFavoriteAccount>();
		CustomerFavoriteAccount fav = new CustomerFavoriteAccount();
		Account account = new Account();
		account.setAccountId(1);
		Customer customer = new Customer();
		customer.setCustomerId(1);
		fav.setAccount(account);
		fav.setFavoriteAccountId(1);
		fav.setCustomer(customer);
		customerFavAccout.add(fav);
		try {
			doReturn(customerFavAccout).when(customerServiceImpl).getCustomerFavoriteAccountDetails(1, 0, 3);
			customerController.getCustomerAccountDetails(1, 0, 3);
			assertEquals("1", customerFavAccout.get(0).getFavoriteAccountId().toString());
		} catch (CustomerAccountNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testdeleteFavAccountDetails() {
		AppResponseDto respnse = new AppResponseDto();
		respnse.setStatusCode("1001");
		doReturn(respnse).when(customerServiceImpl).deleteFavAccountDetails(1, 1);
		customerController.deleteFavAccountDetails(1, 1);
		assertEquals("1001", respnse.getStatusCode());
	}

}
